/**
 * Created by JMendones on 8/28/15.
 */
/**
 * Created by JMendones on 8/26/15.
 */
Ext.define('BookManager.view.BookHandler', {
//    extend: 'Ext.window.Window',
    extend: 'GlobalComponentEngine.view.GridTemplate',
    alias: 'widget.bmbookhandler',

    requires:[
        'GlobalComponentEngine.view.GridTemplate',
        'BookManager.view.BookHandlerViewController',
        'BookManager.view.BookHandlerViewModel'
    ],

    requires: [
        'i21.view.Filter',
        'i21.view.Statusbar',
        'Ext.form.Panel',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.toolbar.Separator',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.form.field.Text',
        'Ext.grid.View',
        'Ext.selection.CheckboxModel',
        'Ext.grid.plugin.CellEditing',
        'BookManager.model.BookHandler',
        'BookManager.store.BookHandler'
    ],

    height: 480,
    minHeight: 480,
    minWidth: 600,
    width: 600,
    collapsible: true,
    iconCls: 'small-icon-i21',
    title: 'Book Handlers',
    maximizable: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },


    items: [
        {
            xtype: 'form',
            flex: 1,
            itemId: 'frmBookHandler',
            margin: -1,
            layout: 'fit',
            bodyPadding: 5,
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    itemId: 'tlbMenu',
                    items: [
                        {
                            xtype: 'button',
                            itemId: 'btnSave',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-save',
                            scale: 'large',
                            text: 'Save'
                        },
                        {
                            xtype: 'button',
                            itemId: 'btnUndo',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-undo',
                            scale: 'large',
                            text: 'Undo'
                        },
                        {
                            xtype: 'tbseparator',
                            height: 30
                        },
                        {
                            xtype: 'button',
                            itemId: 'btnClose',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-close',
                            scale: 'large',
                            text: 'Close'
                        }
                    ]
                },
                {
                    xtype: 'istatusbar',
                    itemId: 'stpCountry',
                    dock: 'bottom'
                }
            ],
            items: [
                {
                    xtype: 'advancefiltergrid',//'gridpanel',
                    itemId: 'grdBookHandler',
//                    width: 576,
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            itemId: 'tlbBookHandler',
                            items: [
                                {
                                    xtype: 'button',
                                    itemId: 'btnDelete',
                                    iconCls: 'small-delete',
                                    text: 'Remove'
                                },
                                {
                                    xtype: 'tbseparator'
                                },
                                {
//                                    xtype: 'filtergrid',
//                                    itemId: 'txtFilterGrid'
                                    xtype: 'filtergrid',
                                    itemId: 'txtFilterGrid',
                                    padding: '0 0 0 5',
                                    width: 218,
                                    fieldLabel: 'Filter',
                                    labelWidth: 33
                                }
                            ]
                        }
                    ],
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colControllerId',
//                            dataIndex: 'intControllerId',
                            hidden:true
                        },
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colControllerName',
//                            dataIndex: 'strControllerName',
                            text: 'Name',
                            flex: 1,
                            editor: {
                                xtype: 'textfield',
                                itemId: 'txtControllerName'
                            }
                        },
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colFirstName',
                            width: 83,
//                            dataIndex: 'strFirstName',
                            text: 'First Name',
                            flex: 1,
                            editor: {
                                xtype: 'textfield',
                                itemId: 'txtCatDesc'
                            }
                        },
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colMiddleName',
                            width: 83,
//                            dataIndex: 'strMiddleName',
                            text: 'Middle Name',
                            flex: 1,
                            editor: {
                                xtype: 'textfield',
                                itemId: 'txtMiddleName'
                            }
                        },
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colLastName',
                            width: 83,
//                            dataIndex: 'strLastName',
                            text: 'Last Name',
                            flex: 1,
                            editor: {
                                xtype: 'textfield',
                                itemId: 'txtLastName'
                            }
                        },
                        {
                            xtype: 'datecolumn',
                            dataType:'date',
                            itemId: 'colBirthDate',
                            width: 83,
                            dataIndex: 'dtmBirthDate',
                            text: 'Date of Birth',
                            format:'m/d/Y H:i:s',
                            flex: 1,
                            editor: {
                                xtype: 'datefield',
                                showTime: true
                            }
                        },
                        {
                            xtype: 'checkcolumn',
                            dataType:'boolean',
                            itemId: 'colIsActive',
                            width: 83,
                            dataIndex: 'ysnoIsActive',
                            text: 'Active',
                            flex: 1,
                            editor: {
                                xtype: 'checkboxfield',
                                itemId: 'chkIsActive'
                            }
                        }
                    ],
                    viewConfig: {
                        itemId: 'gvwBookHandler'
                    },
                    selModel: Ext.create('Ext.selection.CheckboxModel', {
                        selType: 'checkboxmodel'
                    }),
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            pluginId: 'cepBookHandler',
                            clicksToEdit: 1
                        })
                    ]
                }
            ]
        }
    ]

});