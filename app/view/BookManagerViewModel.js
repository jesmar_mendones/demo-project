/**
 * Created by JMendones on 8/20/15.
 */


Ext.define('BookManager.view.BookManagerViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.bmbookmanager',

    requires: [
        'BookManager.store.BookManager',
        'BookManager.store.BufferedBookCategory',
        'BookManager.store.BookCategory',
        'BookManager.store.Users',
        'BookManager.store.BookHandler'
    ],

    //TODO: create buffered store for reviews
    stores: {
        bookcategory:{
            type:'bmbookcategory'//'bmbufferedbookcategory'
        },

        rating: {
            autoLoad: true,
            data : [
                {"rating":"1"},{"rating":"2"},{"rating":"3"},{"rating":"4"},{"rating":"5"},
                {"rating":"6"},{"rating":"7"},{"rating":"8"},{"rating":"9"},{"rating":"10"}
            ],
            fields: {
                name: 'rating'
            }
        },

        users:{
            type:'bmusers'
        },

        bmcontroller:{
            type:'bmbookhandler'
        }
    }

});