/**
 * Created by JMendones on 8/28/15.
 */

Ext.define('BookManager.view.BookHandlerViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.bmbookhandler',

    requires:['BookManager.store.BookHandler'],

    stores:{
        bmbookhandler: {type:'bmbookhandler'}
    }
});