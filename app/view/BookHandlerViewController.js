/**
 * Created by JMendones on 8/28/15.
 */

Ext.define('BookManager.view.BookHandlerViewController',{
    extend:'Ext.app.ViewController',
    alias:'controller.bmbookhandler',


    config:{
        binding:{
            grdBookHandler:{
                colControllerId:{
                    dataIndex:'intControllerId'
                },
                colControllerName:{
                    dataIndex:'strControllerName'
                },
                colFirstName:{
                    dataIndex:'strFirstName'
                },
                colMiddleName:{
                    dataIndex:'strMiddleName'
                },
                colLastName:{
                    dataIndex:'strLastName'
                },
                colBirthDate:{
                    dataIndex:'dtmBirthDate'
                },
                colIsActive:{
                    dataIndex:'ysnoIsActive'
                }

            }
        }
    },

    setupContext: function(options){
        var me = this,
//            win = me.view,
            win = me.view,//options.window,
            store = Ext.create('BookManager.store.BookHandler');

        var grdBookHandler = win.down('#grdBookHandler');

        win.context = Ext.create('iRely.mvvm.Engine',{
            window:win,
            store:store,
            binding:me.config.binding,
            singleGridMgr:Ext.create('iRely.mvvm.grid.Manager',{
                grid:grdBookHandler,
//                grid:me.view.down('grid'),
                title:'Handler',
                deleteButton:grdBookHandler.down('#btnDelete')//,
//                columns: [
//                        {
//                            xtype: 'gridcolumn',
//                            itemId: 'colControllerId',
//                            dataIndex: 'intControllerId',
//                            hidden:true
//                        },
//                        {
//                            xtype: 'gridcolumn',
//                            itemId: 'colControllerName',
//                            dataIndex: 'strControllerName',
//                            text: 'Name',
//                            flex: 1,
//                            editor: {
//                                xtype: 'textfield',
//                                itemId: 'txtControllerName'
//                            }
//                        },
//                        {
//                            xtype: 'gridcolumn',
//                            itemId: 'colFirstName',
////                            width: 83,
//                            dataIndex: 'strFirstName',
//                            text: 'First Name',
//                            flex: 1,
//                            editor: {
//                                xtype: 'textfield',
//                                itemId: 'txtCatDesc'
//                            }
//                        },
//                        {
//                            xtype: 'gridcolumn',
//                            itemId: 'colMiddleName',
////                            width: 83,
//                            dataIndex: 'strMiddleName',
//                            text: 'Middle Name',
//                            flex: 1,
//                            editor: {
//                                xtype: 'textfield',
//                                itemId: 'txtMiddleName'
//                            }
//                        },
//                        {
//                            xtype: 'gridcolumn',
//                            itemId: 'colLastName',
////                            width: 83,
//                            dataIndex: 'strLastName',
//                            text: 'Last Name',
//                            flex: 1,
//                            editor: {
//                                xtype: 'textfield',
//                                itemId: 'txtLastName'
//                            }
//                        },
//                        {
//                            xtype: 'datecolumn',
//                            itemId: 'colBirthDate',
////                            width: 83,
//                            dataIndex: 'dtmBirthDate',
//                            text: 'Date of Birth',
//                            format:'m/d/Y',
//                            flex: 1,
//                            editor: {
//                                xtype: 'datefield'
//                            }
//                        },
//                        {
//                            xtype: 'checkcolumn',
//                            itemId: 'colIsActive',
////                            width: 83,
//                            dataIndex: 'ysnoIsActive',
//                            text: 'Active',
//                            flex: 1,
//                            editor: {
//                                xtype: 'checkboxfield',
//                                itemId: 'chkIsActive'
//                            }
//                        }
//                    ]
            })
        });



        return win.context;
    },

    show:function(){
        var me = this;
        var win = me.view;
        win.helpURL = "";
        win.show();

        var context = me.setupContext();
        context.data.load();
    },

    init: function (application) {
        this.control({

        });
    }



});