/**
 * Created by JMendones on 8/26/15.
 */
Ext.define('BookManager.view.BookCategory', {
    extend: 'Ext.window.Window',
    alias: 'widget.bmbookcategory',

    requires: [
        'i21.view.Filter',
        'i21.view.Statusbar',
        'Ext.form.Panel',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.toolbar.Separator',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.form.field.Text',
        'Ext.grid.View',
        'Ext.selection.CheckboxModel',
        'Ext.grid.plugin.CellEditing'
    ],

    height: 480,
    minHeight: 480,
    minWidth: 600,
    width: 600,
    collapsible: true,
    iconCls: 'small-icon-i21',
    title: 'Book Category',
    maximizable: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },


    items: [
        {
            xtype: 'form',
            flex: 1,
            itemId: 'frmBookCategory',
            margin: -1,
            layout: 'fit',
            bodyPadding: 5,
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    itemId: 'tlbMenu',
                    items: [
                        {
                            xtype: 'button',
                            itemId: 'btnSave',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-save',
                            scale: 'large',
                            text: 'Save'
                        },
                        {
                            xtype: 'button',
                            itemId: 'btnUndo',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-undo',
                            scale: 'large',
                            text: 'Undo'
                        },
                        {
                            xtype: 'tbseparator',
                            height: 30
                        },
                        {
                            xtype: 'button',
                            itemId: 'btnClose',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-close',
                            scale: 'large',
                            text: 'Close'
                        }
                    ]
                },
                {
                    xtype: 'istatusbar',
                    itemId: 'stpCountry',
                    dock: 'bottom'
                }
            ],
            items: [
                {
                    xtype: 'gridpanel',
                    itemId: 'grdBookCategory',
                    width: 576,
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            itemId: 'tlbBookCategory',
                            items: [
                                {
                                    xtype: 'button',
                                    itemId: 'btnDelete',
                                    iconCls: 'small-delete',
                                    text: 'Remove'
                                },
                                {
                                    xtype: 'tbseparator'
                                },
                                {
                                    xtype: 'filtergrid',
                                    itemId: 'txtFilterGrid'
                                }
                            ]
                        }
                    ],
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colCategoryId',
                            dataIndex: 'intCategoryId',
                            hidden:true
                        },
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colCategoryName',
                            dataIndex: 'strName',
                            text: 'Name',
                            flex: 1,
                            editor: {
                                xtype: 'textfield',
                                itemId: 'txtCatName'
                            }
                        },
                        {
                            xtype: 'gridcolumn',
                            itemId: 'colDescription',
                            width: 83,
                            dataIndex: 'strDescription',
                            text: 'Description',
                            flex: 1,
                            editor: {
                                xtype: 'textfield',
                                itemId: 'txtCatDesc'
                            }
                        }
                    ],
                    viewConfig: {
                        itemId: 'gvwBookCategory'
                    },
                    selModel: Ext.create('Ext.selection.CheckboxModel', {
                        selType: 'checkboxmodel'
                    }),
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            pluginId: 'cepBookCategory',
                            clicksToEdit: 1
                        })
                    ]
                }
            ]
        }
    ]

});