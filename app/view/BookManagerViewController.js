/**
 * Created by JMendones on 8/20/15.
 */

Ext.define('BookManager.view.BookManagerViewController',{
    extend:'Ext.app.ViewController',
    alias:'controller.bmbookmanager',

    requires:[
        'Ext.window.MessageBox'
    ],

    config:{
        searchConfig: {
            title:  'Search Book',
            type: 'BookManager.BookManager',
            api: {
                read: '../BookManager/api/BookManager2/SearchBookManager'//2/Search'
            },
            columns: [
                {dataIndex: 'intBookId',text: "Book Id", flex: 1, defaultSort:true, dataType: 'numeric', key: true, hidden: false},
                {dataIndex: 'strTitle', text: 'Title', flex: 2,  dataType: 'string'},
                {dataIndex: 'strAuthor', text: 'Author', flex: 1,  dataType: 'string'},
                {dataIndex: 'strManufacturer', text: 'Manufacturer', flex: 1,  dataType: 'string'},
//                {dataIndex: 'intCategory', text: 'Category', flex: 1,  dataType: 'int'},
                {dataIndex: 'strCategoryName', text: 'Category', flex: 1,  dataType: 'string'}
            ]
        },
        binding: {
            bind: {
                title: 'Book  - {current.strTitle}'
            },
            txtTitle: '{current.strTitle}',
            txtAuthor: '{current.strAuthor}',
            txtManufacturer: '{current.strManufacturer}',
//            txtCategory: '{current.intCategory}',
            cboCategory:{
                value:'{current.intCategory}',
                store:'{bookcategory}'
            },

            grdReviews: {
                colId: 'intId',
                colHandler:{
                    dataIndex: 'strControllerName',//'intControllerId',
                    editor:{
                        store:'{bmcontroller}'//,
                        //bug, status is edited in different row, for now use onHandlerSelect
//                        origValueField:'intControllerId',
//                        origUpdateField:'intControllerId'
                    }
                },
                colBookId: 'intBookId',
                colReviewerName: 'strReviewerName',
                colContent: 'strContent',
//                colRating: 'intRating',
                colRating:{
                    dataIndex:'intRating',
                    editor: {
                        store: '{rating}'
                    }
                },
                colDateSubmitted: 'dtmDateSubmitted'
            }
        }
    },
    setupContext : function(options){
        "use strict";
        var me = this,
            win = options.window,
            store = Ext.create('BookManager.store.BookManager', { pageSize: 1 });

        var grdReviews = win.down('#grdReviews');

        win.context = Ext.create('iRely.mvvm.Engine', {
            binding: me.config.binding,
            window : win,
            store  : store,
            include: 'tblBMReview, ' +
                'tblBMReview.tblBMHandler',
//            validateRecord:me.validateRecord,
//            createRecord:me.createRecord,
            enableAudit: true,
            enableComment: true,
            attachment: Ext.create('iRely.mvvm.attachment.Manager',{
                type:'BookManager.BookManager',
                window:win
            }),
            details: [
                {
                    key: 'tblBMReview',
                    component: Ext.create('iRely.mvvm.grid.Manager', {
                        grid: win.down('#grdReviews'),
                        deleteButton : grdReviews.down('#btnDeleteBookReview')
                    })
                }
            ]
        });

        grdReviews.editingPlugin.on({
            beforeedit:me.onCellEditingGrdReviewsBeforeEdit
        })

        return win.context;
    },
    show : function(config) {
        "use strict";

        var me = this,
            win = this.getView();

        if (config) {
            win.show();

            var context = me.setupContext( {window : win} );

            if (config.action === 'new') {
                context.data.addRecord();
            } else {
                if (config.id) {
                    config.filters = [{
                        column: 'BookId',
                        value: config.id
                    }];
                }
                context.data.load({
                    filters: config.filters
                });
            }
        }
    },
    init: function(application) {
        this.control({
            "#btnSave": {
                click: this.btnSaveClick
            },
            "#grdReviews":{
//                afterlayout: this.onAfterLayout
            },

            "#cboHandler" :{
                select: this.onHandlerSelect
            }

        });
    },

    onCellEditingGrdReviewsBeforeEdit : function(editor, context, eOpts){
        var ctx = context,
            record = context.record,
            me = this,
            vm = me.view.up('window').getViewModel();

        if(record.phantom){
            if(record.data.strControllerName === '' && ctx.field != 'strControllerName')
                return false;
        }

    },

    createRecord:function(config,action){
        Ext.Msg.alert('create Record');
        action();
    },
    validateRecord:function(config,action){
        Ext.Msg.alert('before');
        this.validateRecord(config, function(result) {
            Ext.Msg.alert('after');
        });

    },
    onAfterLayout:function(grid){
//        Ext.Msg.alert('afterlayout');
        var column = grid.columns[1];
        var id = column.getId();
        var cells = Ext.DomQuery.select('.x-grid-cell-'+id);
        column.addCls('wrap-text');
        for (var i = 0; i < cells.length; i++){
            if (column.hidden === true) {
                console.log('hidden');
                Ext.fly(cells[i]).removeCls('wrap-text');
            } else {
                console.log('show');
                Ext.fly(cells[i]).addCls('wrap-text');
            }
        }
    },

    btnSaveClick:function(button, e, eOpts){
        var win = button.up("window");
        var store = win.down('#grdReviews').getStore();
//        store.sync({
//            success:function(){
//                Ext.Msg.alert('saved');
//            }
//        })
    },

    onHandlerSelect:function(combo, records, eOpts){
        if(records.length <= 0) return;

        var grid = combo.up('grid'),
            plugin = grid.getPlugin('cepBookManager'),
            current = plugin.getActiveRecord();

        if(combo.itemId == 'cboHandler'){
            current.set('intControllerId', records[0].get('intControllerId'));
        }
    }
});

