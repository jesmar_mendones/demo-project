/**
 * Created by JMendones on 9/2/15.
 */

Ext.define('BookManager.view.RecurringTransactionViewController',{
    extend:'Ext.app.ViewController',
    alias:'controller.bmrecurringtransaction',

    config:{
        helpURL:'/display/DOC/SM+Recurring+Transactions',
        binding:{
            grdRecurring:{
                colTransactionType:{
                    dataIndex:'strTransactionType',
                    editor:{
                        store: '{transactionType}',
                        value: '{currentType}'
                    }
                },
                colAssignedUser:{
                    dataIndex: 'strAssignedUser',
                    editor: {
                        store: '{userSecurity}',
                        origValueField: 'intEntityId',
                        origUpdateField: 'intEntityId'
                    }
                },
                colWarningDays:{
                    dataIndex:'intWarningDays'
                },

                colFrequency: {
                    dataIndex: 'strFrequency',
                    editor: {
                        store: '{frequency}'
                    }
                },

                colLastProcess:{
                    dataIndex: 'dtmLastProcess'
                },

                colNextProcess:{
                    dataIndex:'dtmNextProcess'
                },
                colDue:{
                    dataIndex:'ysnDue'
                },
                colGroup:{
                    dataIndex: 'intGroupId'
                },
                colDayOfMonth:{
                    dataIndex: 'strDayOfMonth',
                    editor: {
                        store: '{dayOfMonth}'
                    }
                },
                colStartDate:{
                    dataIndex: 'dtmStartDate'
                },
                colEndDate: {
                    dataIndex: 'dtmEndDate'
                },
                colActive: {
                    dataIndex: 'ysnActive'
                },
                colIteration:{
                    dataIndex: 'intIteration'
                }

            }
        }
    },

    setupContext: function(options){
        var me = this,
            win = options.window,
            store = this.getViewModel().getStore('recurringTransaction'),
            gridRecurring = win.down('#grdRecurring');

        var context = Ext.create('iRely.mvvm.Engine',{
            window:win,
            store:store,
            binding:me.config.binding,
            singleGridMgr:Ext.create('iRely.mvvm.grid.Manager',{
                grid:gridRecurring,
                deletebutton:win.down('#btnDelete')//,
//                createRecord:me.createRecord
            })

        })

//        // Subscribe in store object
//        store.on({
//            update: me.onStoreUpdate,
//            scope: me
//        });
//
        gridRecurring.editingPlugin.on({
            beforeedit: me.onCellEditingGrdBeforeEdit
        });

        win.context = context;

        return context;
    },

    show:function(config){
        var me = this,
            win = me.getView(),
            vm = win.getViewModel(),
            cboTransactionType = win.down('#cboTransactionType'),
            context,
            _filters,
            param = config.param;

        win.show();

        me.context = me.setupContext({window: win});

//        vm.set('currentType', (!config.param || !config.param.type) ? 'Invoice' : config.param.type);
        vm.bind('{recurringTransaction}', function(){
            if(param !== null) {
                var grd = me.getView().down('#grdRecurring');
                if (config.param.type !== undefined) {
                    var defaultFilters = [
                        {
                            column: 'strTransactionType',
                            condition: 'eq',
                            conjunction: 'And',
                            value: config.param.type,
                            displayCondition: 'Equals'
                        }
                    ];

                    grd.applyFilters(defaultFilters);
                }
                else if(config.param.filterConfig !== undefined){
                    var defaultFilters = [];
                    for(var i in config.param.filterConfig){
                        defaultFilters.push(config.param.filterConfig[i]);
                    }

                    grd.applyFilters(defaultFilters);
                }
            }
        });


//        if(param !== null){
//            if(param.type !== undefined){
//                vm.set('reminderType', config.param.type);
//            }
//        }

//        me.doLoadData(vm.get('currentType'));

        me.context.data.load();

    },

    init:function(){
        this.control({

        })
    },

    onCellEditingGrdBeforeEdit : function(editor, context, eOpts){
        var ctx = context,
            record = context.record,
            me = this,
            vm = me.view.up('window').getViewModel();

        if(record.phantom){
            if((record.data.strTransactionType === undefined || record.data.strTransactionType === null)  && ctx.field !== 'strTransactionType')
                return false;
        }


        vm.set('recurringtransaction', !vm.get('recurringtransaction'));

        //set the store of cboTransactionNumber
        if(ctx.field == 'strTransactionNumber'){
            var _transType = ctx.record.get('strTransactionType');

            var cboStore = function(){
                var _store;
                switch (_transType.toUpperCase()) {
                    case 'INVOICE':
                        _store = vm.get('invoiceStore')
                        break;
                    case 'GENERAL JOURNAL':
                        _store = vm.get('generalJournalStore')
                        break;
                    case 'BILL':
                        _store = vm.get('billStore')
                        break;
                    case 'PURCHASE ORDER':
                        _store = vm.get('purchaseOrderStore')
                        break;
                    case 'BILL TEMPLATE':
                        _store = vm.get('billTemplateStore');
                        break;
                }
                return _store;
            };

            var cboDisplayValueField = function(){
                switch (_transType.toUpperCase()){
                    case "BILL":
                        return 'strBillId';
                        break;
                    case "BILL TEMPLATE":
                        return 'strBillId';
                        break;
                    case 'PURCHASE ORDER':
                        return 'strPurchaseOrderNumber';
                        break;
                    default:
                        return 'strTransactionNumber';
                        break;
                }
            };

            var cboEditor = {
                xtype: 'gridcombobox',
                columns: [
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'intTransactionId',
                        hidden: true
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'strTransactionNumber',
                        text: 'Transaction Number',
                        flex: 1
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'dtmDate',
                        text: 'Transaction Date'
                    }
                ],
                store: cboStore(),
                itemId: 'cboTransactionNumber',
                displayField: cboDisplayValueField(),
                valueField: cboDisplayValueField()
            };

            ctx.column.setEditor(cboEditor);

        }
    }
})