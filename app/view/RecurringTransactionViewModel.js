/**
 * Created by JMendones on 9/2/15.
 */

Ext.define('BookManager.view.RecurringTransactionViewModel',{
    extend:'Ext.app.ViewModel',
    alias:'viewmodel.bmrecurringtransaction',

    requires:[
        'AccountsReceivable.store.RecurringTransactionBuffered',
        'GeneralLedger.store.BufJournalTemplate',
        'AccountsPayable.store.BillRecur',
        'AccountsPayable.store.BillTemplateRecur',
        'AccountsPayable.store.PurchaseOrderRecur',
        'i21.store.UserSecurityBuffered',
        'BookManager.store.RecurringTransaction'
    ],

    data: {
        currentType: 'Invoice',
        previousType: null,
        recurringtransaction: null,
        reminderType: null
    },

    stores:{
        recurringTransaction : {
            type: 'bmrecurringtransaction'
        },

        transactionType: {
            data: [
                {strTransactionType: 'Invoice', intTransactionTypeId: 1},
                {strTransactionType: 'General Journal', intTransactionTypeId: 2},
                {strTransactionType: 'Bill', intTransactionTypeId: 3},
                {strTransactionType: 'Bill Template', intTransactionTypeId: 4},
                {strTransactionType: 'Purchase Order', intTransactionTypeId: 5}
            ],

            fields:[
                {name: 'strTransactionType', type: 'string'},
                {name: 'intTransactionTypeId', type: 'int'}
            ]
        },

        invoiceStore:{
            type: 'recurringtransactionbuffered',
            remoteFilter: true,
            filters: [
                {
                    condition: 'eq',
                    id: 'strTransactionType',
                    conjunction: 'And',
                    column: 'strTransactionType',
                    value: 'Invoice'
                }
            ]
        },

        generalJournalStore:{
            type: 'glbufjournaltemplate'
        },

        billStore:{
            type: 'billrecur'
        },

        purchaseOrderStore:{
            type: 'porecur'
        },

        billTemplateStore:{
            type: 'billtemplaterecur'
        },

        userSecurity:{
            type: 'smusersecuritybuffered'
        },

        frequency: {
            data: [
                {intFrequencyId: 1, strFrequency: 'Daily'},
                {intFrequencyId: 2, strFrequency: 'Weekly'},
                {intFrequencyId: 3, strFrequency: 'Bi-Weekly'},
                {intFrequencyId: 5, strFrequency: 'Semi-Monthly'},
                {intFrequencyId: 5, strFrequency: 'Monthly'},
                {intFrequencyId: 6, strFrequency: 'Bi-Monthly'},
                {intFrequencyId: 7, strFrequency: 'Quarterly'},
                {intFrequencyId: 8, strFrequency: 'Semi-Annually'},
                {intFrequencyId: 9, strFrequency: 'Annually'}
            ],

            fields: [
                { name: 'intFrequencyId', type: 'int' },
                { name: 'strFrequency', type: 'string' }
            ]
        },
        dayOfMonth: {
            data: [
                { strDayOfMonth: '1' },
                { strDayOfMonth: '2' },
                { strDayOfMonth: '3' },
                { strDayOfMonth: '4' },
                { strDayOfMonth: '5' },
                { strDayOfMonth: '6' },
                { strDayOfMonth: '7' },
                { strDayOfMonth: '8' },
                { strDayOfMonth: '9' },
                { strDayOfMonth: '10' },
                { strDayOfMonth: '11' },
                { strDayOfMonth: '12' },
                { strDayOfMonth: '13' },
                { strDayOfMonth: '14' },
                { strDayOfMonth: '15' },
                { strDayOfMonth: '16' },
                { strDayOfMonth: '17' },
                { strDayOfMonth: '18' },
                { strDayOfMonth: '19' },
                { strDayOfMonth: '20' },
                { strDayOfMonth: '21' },
                { strDayOfMonth: '22' },
                { strDayOfMonth: '23' },
                { strDayOfMonth: '24' },
                { strDayOfMonth: '25' },
                { strDayOfMonth: '26' },
                { strDayOfMonth: '27' },
                { strDayOfMonth: '28' },
                { strDayOfMonth: '29' },
                { strDayOfMonth: '30' },
                { strDayOfMonth: '31' },
                { strDayOfMonth: 'Last Day' }
            ],
            fields:[
                { name: 'strDayOfMonth', type: 'string' }
            ]
        }
    }
})