/**
 * Created by JMendones on 9/2/15.
 */

Ext.define('BookManager.view.RecurringTransaction',{
    extend:'Ext.window.Window',
    alias:'widget.bmrecurringtransaction',

    requires:[
        'Ext.form.Panel',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.toolbar.Separator',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.form.field.Number',
        'Ext.grid.column.Date',
        'Ext.form.field.Date',
        'Ext.grid.column.Boolean',
        'Ext.grid.column.Check',
        'Ext.grid.column.Number',
        'Ext.grid.View',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CheckboxModel'
    ],

    height: 630,
    hidden: false,
    itemId: 'bmrecurringtransaction',
    width: 1141,
    layout: 'fit',
    collapsible: true,
    iconCls: 'small-icon-i21',
    title: 'Recurring Transactions',
    maximizable: true,

    items:[
        {
            xtype: 'form',
            height: 350,
            itemId: 'frmRecurringTransaction',
            margin: -1,
            width: 450,
            bodyBorder: false,
            bodyPadding: 5,
            header: false,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    formBind: false,
                    itemId: 'tlbRecurringTransaction',
                    width: 588,
                    layout: {
                        type: 'hbox',
                        padding: '0 0 0 1'
                    },
                    items: [
                        {
                            xtype: 'button',
                            formBind: true,
                            tabIndex: -1,
                            height: 57,
                            hidden: true,
                            itemId: 'btnNew',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-new',
                            scale: 'large',
                            text: 'New'
                        },
                        {
                            xtype: 'button',
                            formBind: true,
                            tabIndex: -1,
                            height: 57,
                            itemId: 'btnSave',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-save',
                            scale: 'large',
                            text: 'Save'
                        },
                        {
                            xtype: 'button',
                            tabIndex: -1,
                            height: 57,
                            itemId: 'btnUndo',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-undo',
                            scale: 'large',
                            text: 'Undo'
                        },
                        {
                            xtype: 'button',
                            formBind: true,
                            tabIndex: -1,
                            height: 57,
                            itemId: 'btnPrint',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-print',
                            scale: 'large',
                            text: 'Print'
                        },
                        {
                            xtype: 'tbseparator',
                            height: 30
                        },
                        {
                            xtype: 'button',
                            tabIndex: -1,
                            height: 57,
                            itemId: 'btnProcess',
                            width: 55,
                            iconAlign: 'top',
                            iconCls: 'large-tools',
                            scale: 'large',
                            text: 'Process'
                        },
                        {
                            xtype: 'button',
                            tabIndex: -1,
                            height: 57,
                            itemId: 'btnHistory',
                            width: 52,
                            iconAlign: 'top',
                            iconCls: 'large-history',
                            scale: 'large',
                            text: 'History'
                        },
                        {
                            xtype: 'tbseparator',
                            height: 30
                        },
                        {
                            xtype: 'button',
                            tabIndex: -1,
                            height: 57,
                            itemId: 'btnClose',
                            width: 45,
                            iconAlign: 'top',
                            iconCls: 'large-close',
                            scale: 'large',
                            text: 'Close'
                        }
                    ]
                },
                {
                    xtype: 'istatusbar',
                    flex: 1,
                    dock: 'bottom',
                    itemId: 'tlbPagingStatus'
                }
            ],

            items: [
                {
                    xtype: 'container',
                    hidden: true,
                    margin: '5 0 10 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            isFormField: false,
                            reference: 'cboTransactionType',
                            itemId: 'cboTransactionType',
                            width: 282,
                            fieldLabel: 'Transaction Type',
                            validateOnChange: false,
                            displayField: 'strTransactionType',
                            queryMode: 'local',
                            valueField: 'strTransactionType'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'advancefiltergrid',
                            reference: 'grdRecurring',
                            itemId: 'grdRecurring',
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    itemId: 'tlbGridRecurring',
                                    layout: {
                                        type: 'hbox',
                                        padding: '0 0 0 1'
                                    },
                                    items: [
                                        {
                                            xtype: 'button',
                                            tabIndex: -1,
                                            itemId: 'btnInsert',
                                            iconCls: 'small-add',
                                            text: 'Insert'
                                        },
                                        {
                                            xtype: 'button',
                                            reference: 'btnDelete',
                                            tabIndex: -1,
                                            itemId: 'btnDelete',
                                            iconCls: 'small-delete',
                                            text: 'Remove'
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'button',
                                            tabIndex: -1,
                                            itemId: 'btnSelectDue',
                                            margin: '0 0 0 5',
                                            iconCls: 'small-select-all',
                                            text: 'Select Due'
                                        },
                                        {
                                            xtype: 'button',
                                            tabIndex: -1,
                                            itemId: 'btnClearAll',
                                            iconCls: 'small-select-none',
                                            text: 'Clear All'
                                        }
                                    ]
                                }
                            ],
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    itemId: 'colTransactionType',
                                    width: 140,
                                    dataIndex: 'strTransactionType',
                                    text: 'Transaction Type',
                                    editor: {
                                        xtype: 'combobox',
                                        itemId: 'cboColTransactionType',
                                        displayField: 'strTransactionType',
                                        forceSelection: true,
                                        valueField: 'strTransactionType'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        var newvalue;
                                        if (!record.phantom && !record.get('ysnAvailable')) {
                                            newvalue = "<span style='color:red;text-decoration:line-through'>"+value+"</span>";
                                        }
                                        else {
                                            newvalue = value;
                                        }
                                        return newvalue;
                                    },
                                    itemId: 'colTransactionNumber',
                                    dataIndex: 'strTransactionNumber',
                                    text: 'Transaction No.',
                                    editor: {
                                        xtype: 'gridcombobox',
                                        columns: [
                                            {
                                                xtype: 'gridcolumn',
                                                dataIndex: 'intTransactionId',
                                                hidden: true
                                            },
                                            {
                                                xtype: 'gridcolumn',
                                                dataIndex: 'strTransactionNumber',
                                                text: 'Transaction Number',
                                                flex: 1
                                            },
                                            {
                                                xtype: 'gridcolumn',
                                                dataIndex: 'dtmDate',
                                                text: 'Transaction Date'
                                            }
                                        ],
                                        itemId: 'cboTransactionNumber',
                                        displayField: 'strTransactionNumber',
                                        valueField: 'strTransactionNumber'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    itemId: 'colReference',
                                    dataIndex: 'strReference',
                                    text: 'Reference',
                                    editor: {
                                        xtype: 'textfield',
                                        itemId: 'txtRecurringReference'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    itemId: 'colAssignedUser',
                                    dataIndex: 'strAssignedUser',
                                    text: 'Assigned User',
                                    editor: {
                                        xtype: 'gridcombobox',
                                        columns: [
                                            {
                                                xtype: 'gridcolumn',
                                                dataIndex: 'strUserName',
                                                text: 'User Id',
                                                flex: 1
                                            },
                                            {
                                                xtype: 'gridcolumn',
                                                dataIndex: 'strFullName',
                                                text: 'Full Name'
                                            },
                                            {
                                                xtype: 'gridcolumn',
                                                dataIndex: 'intUserSecurityID',
                                                hidden: true
                                            },
                                            {
                                                xtype: 'gridcolumn',
                                                dataIndex: 'intEntityId',
                                                hidden: true
                                            }
                                        ],
                                        itemId: 'cboAssignedUser',
                                        displayField: 'strFullName',
                                        valueField: 'strFullName'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    format: '0,000',
                                    itemId: 'colWarningDays',
                                    dataIndex: 'intWarningDays',
                                    text: 'Remind in Advance',
                                    editor: {
                                        xtype: 'numberfield',
                                        itemId: 'txtWarningDays',
                                        allowBlank: false,
                                        allowDecimals: false,
                                        allowExponential: false,
                                        minValue: 0
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    itemId: 'colFrequency',
                                    width: 100,
                                    dataIndex: 'strFrequency',
                                    text: 'Frequency',
                                    editor: {
                                        xtype: 'combobox',
                                        itemId: 'cboFrequency',
                                        displayField: 'strFrequency',
                                        forceSelection: true,
                                        queryMode: 'local',
                                        valueField: 'strFrequency'
                                    }
                                },
                                {
                                    xtype: 'datecolumn',
                                    itemId: 'colLastProcess',
                                    width: 74,
                                    dataIndex: 'dtmLastProcess',
                                    text: 'Last Process'
                                },
                                {
                                    xtype: 'datecolumn',
                                    itemId: 'colNextProcess',
                                    width: 78,
                                    dataIndex: 'dtmNextProcess',
                                    text: 'Next Process',
                                    editor: {
                                        xtype: 'datefield',
                                        itemId: 'dtmNextProcess'
                                    }
                                },
                                {
                                    xtype: 'booleancolumn',
                                    dataType: 'boolean',
                                    itemId: 'colDue',
                                    width: 43,
                                    align: 'center',
                                    dataIndex: 'ysnDue',
                                    text: 'Due',
                                    falseText: 'No',
                                    trueText: 'Yes'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    itemId: 'colGroup',
                                    dataIndex: 'intGroupId',
                                    text: 'Group'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    itemId: 'colDayOfMonth',
                                    width: 80,
                                    dataIndex: 'strDayOfMonth',
                                    text: 'Day of Month',
                                    editor: {
                                        xtype: 'gridcombobox',
                                        columns: [
                                            {
                                                dataIndex: 'strDayOfMonth',
                                                text: 'Day of Month',
                                                flex: 1
                                            }
                                        ],
                                        itemId: 'cboDayOfMonth',
                                        width: 100,
                                        displayField: 'strDayOfMonth',
                                        forceSelection: true,
                                        queryMode: 'local',
                                        valueField: 'strDayOfMonth'
                                    }
                                },
                                {
                                    xtype: 'datecolumn',
                                    itemId: 'colStartDate',
                                    width: 70,
                                    dataIndex: 'dtmStartDate',
                                    text: 'Start Date',
                                    editor: {
                                        xtype: 'datefield',
                                        itemId: 'dtmStartDate'
                                    }
                                },
                                {
                                    xtype: 'datecolumn',
                                    itemId: 'colEndDate',
                                    width: 70,
                                    dataIndex: 'dtmEndDate',
                                    text: 'End Date',
                                    editor: {
                                        xtype: 'datefield',
                                        itemId: 'dtmEndDate'
                                    }
                                },
                                {
                                    xtype: 'checkcolumn',
                                    dataType: 'boolean',
                                    itemId: 'colActive',
                                    width: 52,
                                    dataIndex: 'ysnActive',
                                    text: 'Active'
                                },
                                {
                                    xtype: 'numbercolumn',
                                    itemId: 'colIteration',
                                    width: 81,
                                    align: 'right',
                                    dataIndex: 'intIteration',
                                    text: 'Iterations',
                                    format: '0,000',
                                    editor: {
                                        xtype: 'numberfield',
                                        itemId: 'txtIteration',
                                        allowBlank: false,
                                        allowDecimals: false,
                                        allowExponential: false,
                                        minValue: 1
                                    }
                                }
                            ],
                            viewConfig: {
                                itemId: 'grvRecurring'
                            },
                            plugins: [
                                Ext.create('Ext.grid.plugin.CellEditing', {
                                    pluginId: 'celleditRecurring',
                                    clicksToEdit: 1
                                })
                            ],
                            selModel: {
                                selType: 'checkboxmodel'
                            }
                        }
                    ]
                }
            ]
        }

    ]
})