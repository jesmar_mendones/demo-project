/**
 * Created by JMendones on 8/26/15.
 */
Ext.define('BookManager.view.BookCategoryViewController',{
    extend:'Ext.app.ViewController',
    alias:'controller.bmbookcategory',


    config:{
        binding:{
            grdBookCategory:{
                colCategoryId:{
                    dataIndex:'intCategoryId'
                },
                colCategoryName:{
                    dataIndex:'strName'
                },
                colDescription:{
                    dataIndex:'strDescription'
                }

            }
        }
    },

    setupContext: function(){
        var me = this,
            win = me.view,
            store = Ext.create('BookManager.store.BookCategory');

        var grdBookCategory = win.down('#grdBookCategory');

        win.context = Ext.create('iRely.Engine',{
            window:win,
            store:store,
            binding:me.config.binding,
            singleGridMgr:Ext.create('iRely.mvvm.grid.Manager',{
                grid:grdBookCategory,//win.down('#grdBookCategory'),
                deleteButton:grdBookCategory.down('#btnDelete')
            })
        });

        return win.context;
    },

    show:function(){
        var me = this;
        var win = me.view;
        win.helpURL = "";
        win.show();

        var context = me.setupContext();
        context.data.load();
    },

    init: function (application) {
        this.control({
            '#txtCatName' :{
                change:function(field,newValue,oldValue){
                        field.setValue(newValue.toUpperCase());
                }
            }
        });
    }



});