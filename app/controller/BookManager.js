/**
 * Created by JMendones on 8/20/15.
 */
Ext.define('BookManager.controller.BookManager', {
    extend: 'i21.controller.Module',
    alias: 'controller.bookmanager',

    singleton: true,

    moduleName: 'BookManager',

    controllers: [],

    constructor: function() {
        this.superclass.constructor.call(this);
    }
});