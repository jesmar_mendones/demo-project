/**
 * Created by JMendones on 8/25/15.
 */

Ext.define('BookManager.model.BookCategory', {
    extend: 'iRely.BaseEntity',
    idProperty: 'intCategoryId',
    alias: 'model.bookcategory',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {name: 'intCategoryId', type:'int'},
        {name: 'strName', type:'string'},
        {name: 'strDescription', type:'string'}
    ],
    validators: [
        {
            type: 'presence',
            field: 'strName'
        }
    ]
});