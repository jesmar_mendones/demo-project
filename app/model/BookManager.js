/**
 * Created by JMendones on 8/20/15.
 */
Ext.define('BookManager.model.BookManager',{
    extend: 'iRely.BaseEntity',

    requires:[
        'BookManager.model.BookManagerDetails',
        'Ext.data.Field'
    ],

    idProperty: 'intBookId',

    fields:[
        {name:'intBookId', type:'int'},
        {name:'strTitle', type:'string'},
        {name:'strAuthor',type:'string'},
        {name:'strManufacturer', type:'string'},
        {name:'intCategory',type:'int'}//,
//        {name:'ReviewCount', type:'int'}

    ],

    validators: [
        {type: 'presence', field: 'strTitle'},
        {type: 'presence', field: 'strAuthor'},
        {type: 'presence', field: 'intCategory'}
    ],

    validate:function(options){
        var errors = this.callParent(arguments);

        if(this.get('intCategory') <= 0 ){
            errors.add({
                field: 'intCategory',
                message: 'Please select category.'
            })
        }

        return errors;
    }
});