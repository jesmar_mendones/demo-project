/**
 * Created by JMendones on 9/1/15.
 */

Ext.define('BookManager.model.BookHandler', {
    extend: 'iRely.BaseEntity',
    idProperty: 'intControllerId',
    alias: 'model.bookhandler',

    requires: [
        'Ext.data.Field',
        'BookManager.model.BookManagerDetails'
    ],

    fields: [
        {name: 'intControllerId', type:'int'},
        {name: 'strControllerName', type:'string'},
        {name: 'strFirstName', type:'string'},
        {name: 'strMiddleName', type:'string'},
        {name: 'strLastName', type:'string'},
        {name: 'dtmBirthDate', type:'date',dateFormat: 'c', dateWriteFormat: 'Y-m-d H:i:s'},
        {name: 'ysnoIsActive', type:'boolean'}
    ],
    validators: [
        {
            type: 'presence',
            field: 'strControllerName'
        }
    ]
});