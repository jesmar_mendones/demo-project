/**
 * Created by JMendones on 9/2/15.
 */

Ext.define('BookManager.model.RecurringTransaction',{
    extend:'iRely.BaseEntity',
    alias:'model.bmrecurringtransaction',

    idProperty: 'intRecurringId',
    requires: ['i21.store.UserSecurityBuffered'],
    fields: [
        {name: 'ysnProcess', type: 'bool'},
        {name: 'intRecurringId', type: 'int'},
        {name: 'intTransactionId',type: 'int'},
        {name: 'strTransactionNumber',type: 'string'},
        {name: 'strReference',type: 'string'},
        {name: 'intEntityId',type: 'int'},
        {name: 'strAssignedUser',type: 'string'},
        {name: 'strResponsibleUser',type: 'string'},
        {name: 'intWarningDays',type: 'int'},
        {name: 'strFrequency',type: 'string'},
        {name: 'fltAmount',type: 'float'},
        {name: 'dtmLastProcess',type: 'date',dateFormat: 'c',dateWriteFormat: 'Y-m-d'},
        {name: 'dtmNextProcess',type: 'date',dateFormat: 'c',dateWriteFormat: 'Y-m-d'},
        {name: 'ysnDue',type: 'bool'},
        {name: 'stringGroup',type: 'float'},
        {name: 'strDayOfMonth',type: 'string'},
        {name: 'dtmStartDate',type: 'date',dateFormat: 'c',dateWriteFormat: 'Y-m-d'},
        {name: 'dtmEndDate',type: 'date',dateFormat: 'c',dateWriteFormat: 'Y-m-d'},
        {name: 'ysnActive',type: 'bool'},
        {name: 'intIterations',type: 'int',allowNull: false},
        {name: 'ysnAvailable',type: 'bool'}
    ],

    validators: [
        { type: 'presence', field: 'intIterations' }
    ]
})