/**
 * Created by JMendones on 8/20/15.
 */


Ext.define('BookManager.model.BookManagerDetails',{
    extend: 'iRely.BaseEntity',

    requires: [
        'Ext.data.Field'
    ],
    idProperty: 'intId',
    fields:[
        {name:'intId', type:'int'},
//        {name:'intBookId', type:'int'},
        {name:'intBookId', type:'int',
            reference:{
                type:'BookManager.model.BookManager',
                inverse:{
                    role:'tblBMReview',
                    storeConfig:{
                        complete:true,
                        sortOnLoad: true,
                        sorters: {
                            direction: 'DESC',
                            property: 'intSort'
                        }
                    }
                }
            }
        },
        {name:'strReviewerName', type:'string'},
        {name:'strContent', type:'string'},
        {name:'intRating', type:'int'},
        {name:'dtmDateSubmitted', type:'date'},
        {name:'strControllerName', type:'string'}
    ],

    validators: [
        {type: 'presence', field: 'strContent'},
        {type: 'presence', field: 'intRating'}
    ],

    validate:function(options){
        var errors = this.callParent(arguments);

        if(this.get('intRating') <= 0 ){
            errors.add({
                field: 'intRating',
                message: 'Please select rating.'
            })
        }

        return errors;
    }


});