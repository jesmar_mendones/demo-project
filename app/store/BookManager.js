/**
 * Created by JMendones on 8/20/15.
 */

Ext.define('BookManager.store.BookManager',{
    extend:'Ext.data.Store',
    alias:'store.bookmanager',
    requires: [
        'BookManager.model.BookManager'
    ],


    constructor: function(cfg){
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
//            autoLoad:true,//{start:0,limit:5},//true,
            model:'BookManager.model.BookManager',
            autoSync: false,
            pageSize:50,
            storeId:'BookManager',
            batchActions:true,
            proxy:{
                type:'rest',
//                url:'../BookManager/api/BookManager',//'/api/book',
                api: {
                    read: '../BookManager/api/BookManager2/GetBook',
                    update: '../BookManager/api/BookManager/Put',
                    create: '../BookManager/api/BookManager2/PostBook',
                    destroy: '../BookManager/api/BookManager/Delete'
                },
//                api: {
//                    read: '../BookManager/api/BookManager/Get',
//                    update: '../BookManager/api/BookManager/Put',
//                    create: '../BookManager/api/BookManager/Post',
//                    destroy: '../BookManager/api/BookManager/Delete'
//                },
//                api: {
//                    create: '../BookManager/api/book',
//                    read: '../BookManager/api/book',
//                    update: '../BookManager/api/book',
//                    destroy: '../BookManager/api/book'
//                },
                reader:{
                    type:'json',
                    rootProperty: 'data',
                    messageProperty: 'message'
//                    idProperty: 'BookId',
//                    root:'Books',
//                    totalProperty:'TotalCount'
                },
                //writer:{ writeAllFields:true }
                writer: {
                    type: 'json',
                    allowSingle: false
                }
            }
        },cfg)]);
    }

})
