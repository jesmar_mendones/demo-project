/**
 * Created by JMendones on 8/27/15.
 */

Ext.define('BookManager.store.Users', {
    extend: 'Ext.data.Store',
    alias: 'store.bmusers',

    requires: [
        'i21.model.UserSecurity',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
//            model: 'i21.model.UserSecurity',
            storeId: 'Users',
            pageSize: 50,
            proxy: {
                type: 'rest',
                api: {
                    read: '../i21/api/UserSecurity/SearchUserSecurities'
                },
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            },
            fields:[
                {name: 'intUserSecurityID', type: 'int'},
                {name : 'strUserName', type: 'string'}
//                {dataIndex : 'strEmail', text: 'Email', flex: 1,  dataType: 'string'},
//                {dataIndex : 'strUserRole', text: 'Role', flex: 1,  dataType: 'string'},
//                {dataIndex : 'ysnActive', text: 'Active', dataType: 'boolean', defaultSort : true, sortOrder: 'DESC', sortPriority: 0, xtype : "checkcolumn", listeners :{beforecheckchange: function() { return false; } }}

            ]
        }, cfg)]);
    }
});