/**
 * Created by JMendones on 9/1/15.
 */


Ext.define('BookManager.store.BookHandler', {
    extend: 'Ext.data.Store',//BufferedStore',
    alias: 'store.bmbookhandler',

    requires: [
        'BookManager.model.BookHandler'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
//            autoLoad: false,
            model: 'BookManager.model.BookHandler',
            storeId: 'BookHandler',
            pageSize: 50,
            batchActions: true,
            remoteFilter: true,
            remoteSort: true,
            proxy: {
                type: 'rest',
                api: {
                    read: '../BookManager/api/BookHandler/Get',
                    update: '../BookManager/api/BookHandler/Put',
                    create: '../BookManager/api/BookHandler/Post',
                    destroy: '../BookManager/api/BookHandler/Delete'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    messageProperty: 'message'
                },

                writer: {
                    type: 'json',
                    allowSingle: false
                }
            }
        }, cfg)]);
    }
});