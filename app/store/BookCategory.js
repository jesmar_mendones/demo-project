/**
 * Created by JMendones on 8/26/15.
 */

Ext.define('BookManager.store.BookCategory', {
    extend: 'Ext.data.Store',
    alias: 'store.bmbookcategory',

    requires: [
        'BookManager.model.BookCategory',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            model: 'BookManager.model.BookCategory',
            storeId: 'BookCategory',
            pageSize: 50,
            proxy: {
                type: 'rest',
                api: {
                    read: '../BookManager/api/BookCategory/Get',
                    update: '../BookManager/api/BookCategory/Put',
                    create: '../BookManager/api/BookCategory/Post',
                    destroy: '../BookManager/api/BookCategory/Delete'
                },
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                },
                writer: {
                    type: 'json',
                    allowSingle: false
                }
            }
        }, cfg)]);
    }
});