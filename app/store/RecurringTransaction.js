/**
 * Created by JMendones on 9/2/15.
 */

Ext.define('BookManager.store.RecurringTransaction',{
    extend:'Ext.data.Store',
    alias:'store.bmrecurringtransaction',

    requires:[
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json',
        'BookManager.model.RecurringTransaction'
    ],

    model:'BookManager.model.RecurringTransaction',
    storeId:'RecurringTransaction',
    pageSize:100,
    proxy:{
        type: 'rest',
        api: {
            read: '../i21/api/recurringtransaction/GetRecurringTransaction',
            create: '../i21/api/recurringtransaction/AddRecurringTransaction',
            update: '../i21/api/recurringtransaction/UpdateRecurringTransaction',
            destroy: '../i21/api/recurringtransaction/DeleteRecurringTransaction'
        },
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            allowSingle: false
        }

    }

})