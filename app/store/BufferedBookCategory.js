/**
 * Created by JMendones on 8/25/15.
 */
Ext.define('BookManager.store.BufferedBookCategory', {
    extend: 'Ext.data.BufferedStore',
    alias: 'store.bmbufferedbookcategory',

    requires: [
        'BookManager.model.BookCategory',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            model: 'BookManager.model.BookCategory',
            storeId: 'BufferedBookCategory',
            pageSize: 50,
            proxy: {
                type: 'rest',
                api: {
                    read: '../BookManager/api/BookCategory/Get'
                },
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                },
                writer: {
                    type: 'json',
                    allowSingle: false
                }
            }
        }, cfg)]);
    }
});