﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using iRely.BookManager.Model;
using iRely.BookManager.BusinessLayer;
using iRely.Common;

namespace iRely.BookManager.WebApi
{
    public class BookCategoryController : BaseApiController<tblBMBookCategory>
    {
        private IBookCategoryBl _bl;

        public BookCategoryController(IBookCategoryBl bl) : base(bl) 
        {
            _bl = bl;
        }
    }
}
