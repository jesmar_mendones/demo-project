﻿using IdeaBlade.Core;
using IdeaBlade.Linq;
using iRely.BookManager.BusinessLayer;
using iRely.BookManager.Model;
using iRely.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Data.Entity;

namespace iRely.BookManager.WebApi.Controllers
{
    public class BookManager2Controller : ApiController
    {
        private BookManagerBrl _bookManagerBrl;

        public BookManager2Controller()
        {
            _bookManagerBrl = new BookManagerBrl();
        }

        [HttpGet]
        [ActionName("SearchBookManager")]
        public HttpResponseMessage SearchBookManager(int start = 0, int limit = 1, int page = 0, string sort = "", string filter = "")
        {
            filter = string.IsNullOrEmpty(filter) ? "" : filter; //if null change to empty

            var searchFilters = JsonConvert.DeserializeObject<IEnumerable<SearchFilter>>(filter);
            var searchSorts = JsonConvert.DeserializeObject<IEnumerable<SearchSort>>(sort);
            var predicate = ExpressionBuilder.True<tblBMBook>();
            //var sortSelector =  ExpressionBuilder.GetSortSelector(searchSorts,"intBookId", "ASC");
            var sortSelector = GetSortSelector(searchSorts, "intBookId", "ASC");

            if (searchFilters != null)
            {
                predicate = ExpressionBuilder.GetPredicateBasedOnSearch<tblBMBook>(searchFilters, true);
            }

            var total = _bookManagerBrl.GetBookCount(predicate);
            var data = _bookManagerBrl.SearchBook(page, start, limit, sortSelector, predicate);


            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                data = data.ToList(),
                total = total
            });

            //return Request.CreateResponse(HttpStatusCode.OK, new SearchResult
            //{
            //    data = data.ToList(),
            //    total = total
            //});
        }


        [HttpGet]
        [ActionName("GetBook")]
        public HttpResponseMessage GetBook(int start = 0, int limit = 1, int page = 0, string sort = "", string filter = "")
        {
            filter = string.IsNullOrEmpty(filter) ? "" : filter; //if null change to empty

            var searchFilters = JsonConvert.DeserializeObject<IEnumerable<SearchFilter>>(filter);
            var searchSorts = JsonConvert.DeserializeObject<IEnumerable<SearchSort>>(sort);
            var predicate = ExpressionBuilder.True<tblBMBook>();
            //var sortSelector =  ExpressionBuilder.GetSortSelector(searchSorts,"intBookId", "ASC");
            var sortSelector = GetSortSelector(searchSorts, "intBookId", "ASC");

            if (searchFilters != null)
            {
                predicate = ExpressionBuilder.GetPredicateBasedOnSearch<tblBMBook>(searchFilters, true);
            }

            var total = _bookManagerBrl.GetBookCount(predicate);
            var data = _bookManagerBrl.GetBook(page, start, limit, sortSelector, predicate);


            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                data = data.ToList(),
                total = total
            });
        }


        [HttpPost]
        public HttpResponseMessage PostBook(IEnumerable<tblBMBook> books, bool continueOnConflict = false)
        {
            var result = _bookManagerBrl.AddBook(books, continueOnConflict);
            string exMessage = result.Exception.Message;

            return Request.CreateResponse(HttpStatusCode.Accepted, new
            {
                data = books,
                success = !result.HasError,
                message = new
                {
                    statusText = exMessage,
                    status = result.Exception.Error,
                    button = result.Exception.Button.ToString()
                }
            });
        }


        #region Helper
        private static CompositeSortSelector GetSortSelector(IEnumerable<SearchSort> searchSorts, string key = "", string direction = "ASC")
        {
            if (searchSorts == null || searchSorts.Count() == 0)
                return SortSelector.Combine(new[] { new SortSelector(key, (direction == "ASC") ? ListSortDirection.Ascending : ListSortDirection.Descending) });

            var defaultSort = searchSorts.FirstOrDefault();
            CompositeSortSelector compositeSort = null;

            List<SortSelector> sorts = new List<SortSelector>() { new SortSelector(defaultSort.property, (defaultSort.direction == "ASC") ? ListSortDirection.Ascending : ListSortDirection.Descending) };

            foreach (var searchSort in searchSorts.Skip(1))
            {
                sorts.Add(new SortSelector(searchSort.property, (searchSort.direction == "ASC") ? ListSortDirection.Ascending : ListSortDirection.Descending));
            }

            if (compositeSort == null)
                return compositeSort = SortSelector.Combine(sorts);
            else
                return compositeSort;
        }
        #endregion
    }
}
