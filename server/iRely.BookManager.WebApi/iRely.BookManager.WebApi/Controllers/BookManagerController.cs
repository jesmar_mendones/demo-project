﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iRely.BookManager.Model;
using iRely.BookManager.BusinessLayer;
using iRely.Common;
using System.Web.Http;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace iRely.BookManager.WebApi
{
    public class BookManagerController :BaseApiController<tblBMBook>
    {
        private IBookManagerBl _bl;


        public BookManagerController(IBookManagerBl bl) 
            :base(bl)
        {
            _bl = bl;
        }

        public override Task<HttpResponseMessage> Search(GetParameter param)
        {
            return base.Search(param);
        }

        public override Task<HttpResponseMessage> Put(IEnumerable<tblBMBook> entities, bool continueOnConflict = false)
        {
            return base.Put(entities, continueOnConflict);
        }
        

    }
}