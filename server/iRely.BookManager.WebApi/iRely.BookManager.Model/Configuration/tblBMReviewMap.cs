﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace iRely.BookManager.Model
{
    public class tblBMReviewMap : EntityTypeConfiguration<tblBMReview>
    {
        public tblBMReviewMap()
        {
            this.HasKey(t => t.intId);
            this.Property(t => t.intId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.ToTable("tblBMReview");

            this.Property(t => t.intBookId).HasColumnName("intBookId");
            this.Property(t => t.intControllerId).HasColumnName("intControllerId");
            this.Property(t => t.strReviewerName).HasColumnName("strReviewerName");
            this.Property(t => t.strContent).HasColumnName("strContent");
            this.Property(t => t.intRating).HasColumnName("intRating");
            this.Property(t => t.dtmDateSubmitted).HasColumnName("dtmDateSubmitted");

            //this.HasRequired(t => t.tblBMBook).WithMany(t => t.tblBMReview).HasForeignKey(d => d.intBookId);

            
        }
    }
}