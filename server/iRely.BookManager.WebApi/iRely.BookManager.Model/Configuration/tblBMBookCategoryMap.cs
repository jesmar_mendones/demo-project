﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace iRely.BookManager.Model
{
    public class tblBMBookCategoryMap :EntityTypeConfiguration<tblBMBookCategory>
    {
        public tblBMBookCategoryMap()
        {
            this.HasKey(t => t.intCategoryId);
            this.Property(t => t.intCategoryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.ToTable("tblBMBookCategory");

            this.Property(t => t.strName).HasColumnName("strName");
            this.Property(t => t.strDescription).HasColumnName("strDescription");

            this.HasMany(p => p.tblBMBook)
                .WithRequired(p => p.tblBMBookCategory)
                .HasForeignKey(p => p.intCategory);

            //this.HasMany(p => p.tblBMReview)
            //    .WithRequired(p => p.tblBMCategory)
            //    .HasForeignKey(p => p.intCategoryId);
        }
    }
}
