﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRely.BookManager.Model
{
    public class tblBMHandlerMap : EntityTypeConfiguration<tblBMHandler>
    {
        public tblBMHandlerMap()
        {
            this.HasKey(p => p.intControllerId);
            this.Property(p => p.intControllerId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.ToTable("tblBMHandler");

            this.Property(p => p.strControllerName).HasColumnName("strControllerName");
            this.Property(p => p.strFirstName).HasColumnName("strFirstName");
            this.Property(p => p.strMiddleName).HasColumnName("strMiddleName");
            this.Property(p => p.strLastName).HasColumnName("strLastName");
            this.Property(p => p.dtmBirthDate).HasColumnName("dtmBirthDate");
            this.Property(p => p.ysnoIsActive).HasColumnName("ysnoIsActive");

            this.HasMany(t => t.tblBMReview)
                .WithRequired(t => t.tblBMHandler)
                .HasForeignKey(t => t.intControllerId);

        }
    }
}
