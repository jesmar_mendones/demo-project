﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace iRely.BookManager.Model
{
    public class tblBMBookMap : EntityTypeConfiguration<tblBMBook>
    {
        public tblBMBookMap()
        {
            this.HasKey(t => t.intBookId);
            this.Property(t => t.intBookId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.ToTable("tblBMBook");

            this.Property(t => t.strTitle).HasColumnName("strTitle");
            this.Property(t => t.strAuthor).HasColumnName("strAuthor");
            this.Property(t => t.strManufacturer).HasColumnName("strManufacturer");
            this.Property(t => t.intCategory).HasColumnName("intCategory");

            this.HasMany(p => p.tblBMReview)
                .WithRequired(p => p.tblBMBook)
                .HasForeignKey(p => p.intBookId);
            

        }
    }
}