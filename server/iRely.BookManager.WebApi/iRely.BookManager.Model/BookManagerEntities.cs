﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using iRely.BookManager.Model;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Net;

namespace iRely.BookManager.Model
{
    public partial class BookManagerEntities : DbContext
    {
        static BookManagerEntities()
        {
            Database.SetInitializer<BookManagerEntities>(null);
        }        

        public BookManagerEntities()
            : base(iRely.Common.Security.GetCompanyName())
        {
            Database.SetInitializer<BookManagerEntities>(null);
            this.Configuration.ProxyCreationEnabled = false;
        }

        //public BookManagerEntities(string connection)
        //    : base(connection)
        //{
        //    Database.SetInitializer<BookManagerEntities>(null);
        //    this.Configuration.ProxyCreationEnabled = false;
        //}

        //: base(iRely.Common.Security.GetCompanyName())

        

        public DbSet<tblBMBook> tblBMBooks { get; set; }
        public DbSet<tblBMReview> tblBMReviews { get; set; }
        public DbSet<tblBMBookCategory> tblBMBookCategorys { get; set; }
        public DbSet<tblBMHandler> tblBMHandlers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            

            modelBuilder.Configurations.Add(new tblBMBookMap());
            modelBuilder.Configurations.Add(new tblBMReviewMap());
            modelBuilder.Configurations.Add(new tblBMBookCategoryMap());
            modelBuilder.Configurations.Add(new tblBMHandlerMap());
        }
    }
}