﻿using iRely.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRely.BookManager.Model
{
    public class tblBMHandler : BaseEntity
    {
        public int intControllerId { get; set; }

        public string strControllerName { get; set; }

        public string strFirstName { get; set; }

        public string strMiddleName { get; set; }

        public string strLastName { get; set; }

        public DateTime dtmBirthDate { get; set; }

        public bool ysnoIsActive { get; set; }

        public ICollection<tblBMReview> tblBMReview { get; set; }
    }
}
