﻿using iRely.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRely.BookManager.Model
{
    public class tblBMBookCategory : BaseEntity
    {
        public int intCategoryId { get; set; }
        public string strName { get; set; }
        public string strDescription { get; set; }

        public ICollection<tblBMBook> tblBMBook { get; set; }
        //public ICollection<tblBMReview> tblBMReview { get; set; }
    }
}
