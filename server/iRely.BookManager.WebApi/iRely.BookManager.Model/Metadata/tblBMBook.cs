﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

using iRely.Common;

namespace iRely.BookManager.Model
{
    public class tblBMBook : BaseEntity
    {
        public tblBMBook()
        {

        }

        public int intBookId { get; set; }
        public string strTitle { get; set; }
        public string strAuthor { get; set; }
        public string strManufacturer { get; set; }
        public int intCategory { get; set; }

        public ICollection<tblBMReview> tblBMReview { get; set; }


        private string _strCategoryName;
        [NotMapped]
        public string strCategoryName
        {
            get
            {
                if (string.IsNullOrEmpty(_strCategoryName))
                    if (tblBMBookCategory != null)
                        return tblBMBookCategory.strName;
                    else
                        return null;
                else
                    return _strCategoryName;
            }
            set
            {
                _strCategoryName = value;
            }
        }

        public tblBMBookCategory tblBMBookCategory { get; set; }
    }

    public class vyuBMBook
    {
        public int intBookId { get; set; }
        public string strTitle { get; set; }
        public string strAuthor { get; set; }
        public string strManufacturer { get; set; }
        public string intCategory { get; set; }
    }
}