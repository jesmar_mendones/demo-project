﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iRely.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using iRely.BookManager.Model;

namespace iRely.BookManager.Model
{
    public class tblBMReview:BaseEntity
    {
        public int intId { get; set; }
        
        public int intBookId { get; set; }

        //[DisplayFormat(NullDisplayText="Anonymous")]
        //[NotMapped]
        //private string _strReviewerName;
        //public string strReviewerName {
        //    get
        //    {
        //        if(string.IsNullOrEmpty(strReviewerName))
        //        {
        //            return "Anonymous";
        //        }
        //        else
        //        {
        //            return _strReviewerName;
        //        }
        //    }
        //    set
        //    {
        //        _strReviewerName = value;
        //    }
        //}

        private string _strReviewerName;
        public string strReviewerName {
            get 
            {
                return string.IsNullOrEmpty(_strReviewerName) ? "Anonymous" : _strReviewerName;
            }
            set { _strReviewerName = value; }
        }
        public string strContent { get; set; }
        
        public int intRating { get; set; }
        
        public DateTime dtmDateSubmitted { get; set; }

        public int intControllerId { get; set; }

        public tblBMBook tblBMBook { get; set; }

        public tblBMHandler tblBMHandler { get; set; }

        private string _strControllerName;
        [NotMapped]
        public string strControllerName
        {
            get
            {
                if (string.IsNullOrEmpty(_strControllerName))
                    if (tblBMHandler != null)
                        return tblBMHandler.strControllerName;
                    else
                        return null;
                else
                    return _strControllerName;
            }
            set
            {
                _strControllerName = value;
            }
        }

    }
}