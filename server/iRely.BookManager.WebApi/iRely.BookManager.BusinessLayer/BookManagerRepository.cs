﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iRely.Common;
using iRely.BookManager.Model;
using System.Data.Entity;
using System.Threading.Tasks;


namespace iRely.BookManager.BusinessLayer
{
    public class BookManagerRepository : Repository
    {
        public BookManagerRepository()
            : base(new BookManagerEntities())
        {

        }
    }
}