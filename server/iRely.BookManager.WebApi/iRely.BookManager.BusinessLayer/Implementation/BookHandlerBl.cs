﻿using iRely.BookManager.Model;
using iRely.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRely.BookManager.BusinessLayer
{
    public class BookHandlerBl : BusinessLayer<tblBMHandler>, IBookHandlerBl
    {
        public BookHandlerBl(IRepository db) :base(db)
        {
            _db = db;
        }

        public override void Update(tblBMHandler entity)
        {
            base.Update(entity);
        }
    }
}
