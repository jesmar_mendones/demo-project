﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iRely.Common;
using iRely.BookManager.Model;
using System.Threading.Tasks;
using System.Data.Entity;

namespace iRely.BookManager.BusinessLayer
{
    public class BookManagerBl : BusinessLayer<tblBMBook>, IBookManagerBl
    {
        #region Constructor
        public BookManagerBl(IRepository db) : base(db)
        {
            _db = db;
        }
        #endregion

        public override Task<GetResult<tblBMBook>> Get(GetParameter param)
        {
            var dd =  base.Get(param);            

            dd.Result.data.ForEach(a =>
            {
                a.tblBMReview.ToList().ForEach(b =>
                    {
                        b.strReviewerName = (string.IsNullOrEmpty(b.strReviewerName)) ? "Anonymous" : b.strReviewerName;
                    }
                    );
            });
            
            return dd;
        }

        public override async Task<SearchResult> Search(GetParameter param)
        {
            
            var query = _db.GetQuery<tblBMBook>().Filter(param, true).Include(i => i.tblBMBookCategory);
            //var data = await query.ExecuteProjection(param, "intBookId").ToListAsync();
            var data = await query.ToListAsync();            
            
            return new SearchResult()
            {
                data = data.AsQueryable(),
                total = await query.CountAsync()
            };

        }

        public override Task<BusinessResult<tblBMBook>> SaveAsync(bool continueOnConflict)
        {
            return base.SaveAsync(continueOnConflict);
        }

        public override BusinessResult<tblBMBook> Validate(IEnumerable<tblBMBook> entities, ValidateAction action)
        {
            entities.ToList().ForEach(a =>
            {
                a.tblBMReview.ToList().ForEach(b =>
                {
                    b.dtmDateSubmitted = (b.dtmDateSubmitted.ToString() == "1/1/0001 12:00:00 AM") ? DateTime.Now : b.dtmDateSubmitted; //new
                });
            });
            return base.Validate(entities, action);
        }

        public override void Update(tblBMBook entity, bool updateModifiedFieldsOnly, List<string> modifiedFields = null)
        {
            base.Update(entity, updateModifiedFieldsOnly, modifiedFields);
        }

        public override void UpdateRaw(ProcessRawResult<tblBMBook> rawResult, ValidateAction action)
        {
            base.UpdateRaw(rawResult, action);
        }

        public override ProcessRawResult<tblBMBook> Sync(object content)
        {
            return base.Sync(content);
        }
        
    }
}