﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using iRely.Common;
using iRely.BookManager.Model;

namespace iRely.BookManager.BusinessLayer
{
    public class BookCategoryBl : BusinessLayer<tblBMBookCategory>, IBookCategoryBl
    {
        #region Constructor
        public BookCategoryBl(IRepository db)
            : base(db)
        {
            _db = db;
        }
        #endregion

        public override Task<GetResult<tblBMBookCategory>> Get(GetParameter param)
        {
            return base.Get(param);
        }
    }
}
