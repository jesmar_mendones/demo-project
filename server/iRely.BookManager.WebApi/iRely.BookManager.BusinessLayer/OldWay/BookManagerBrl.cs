﻿using IdeaBlade.Linq;
using IdeaBlade.Core;
using iRely.BookManager.Model;
using iRely.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace iRely.BookManager.BusinessLayer
{
    public class BookManagerBrl : IDisposable
    {
        private Repository _db;

        public BookManagerBrl()
        {
            _db = new Repository(new BookManagerEntities());
        }

        public BookManagerBrl(Repository context)
        {
            _db = context;
        }

        public IQueryable<tblBMBook> SearchBook(int page, int start, int limit, CompositeSortSelector sortSelector, Expression<Func<tblBMBook, bool>> predicate)
        {
            return _db.GetQuery<tblBMBook>()
                .Where(predicate)
                .OrderBySelector(sortSelector)
                .Skip(start)
                .Take(limit)
                .AsNoTracking()
                .Include(i => i.tblBMBookCategory);
        }

        public IQueryable<tblBMBook> GetBook(int page, int start, int limit, CompositeSortSelector sortSelector, Expression<Func<tblBMBook, bool>> predicate)
        {
            return _db.GetQuery<tblBMBook>()
                .Where(predicate)
                .OrderBySelector(sortSelector)
                .Skip(start)
                .Take(limit)
                .AsNoTracking()
                .Include(i => i.tblBMReview)
                .Include("tblBMReview.tblBMHandler");
        }

        public int GetBookCount(Expression<Func<tblBMBook, bool>> predicate)
        {
            return _db.GetQuery<tblBMBook>().Where(predicate).Count();
        }

        public SaveResult AddBook(IEnumerable<tblBMBook> books, bool continueOnConflict = false)
        {
            books.ToList().ForEach(a =>
            {
                a.tblBMReview.ToList().ForEach(b =>
                {
                    b.dtmDateSubmitted = (b.dtmDateSubmitted.ToString() == "1/1/0001 12:00:00 AM") ? DateTime.Now : b.dtmDateSubmitted; //new
                });
            });

            foreach (var book in books)
            {
                _db.AddNew<tblBMBook>(book);
            }

            return _db.Save(continueOnConflict);
        }

        public SaveResult UpdateBook(IEnumerable<tblBMBook> books, bool continueOnConflict = false)
        {
            foreach (var book in books)
            {
                _db.UpdateBatch<tblBMBook>(book);
            }

            return _db.Save(continueOnConflict);
        }


        public void Dispose()
        {
            _db.Dispose();
        }

    }
}
